from socket import *
from enum import Enum
from ipaddress import IPv4Network
from threading import Thread
import logging
import rsa
from time import time, sleep

from rsa import DecryptionError


class SocketChat:
    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, 'instance'):
            cls.instance = _SocketChat(*args, **kwargs)
        return cls.instance


class _Message(Enum):
    HELLO = "HELLO"
    HELLO_BACK = "HELLO_BACK"
    GOODBYE = "GOODBYE"
    KEY = "KEY"
    MESSAGE = "MESSAGE"


class _SocketChat:

    def __init__(self, messaging_port: int, broadcast_port: int, mask_bits: int, username: str, max_connections=4):
        self.encryption_bits = 2048
        self.broadcast_buff_size = 4096
        self.message_buff_size = 8192
        self.encoding = 'utf8'

        self.username = username
        self.messaging_port = messaging_port
        self.broadcast_port = broadcast_port
        self.mask_bits = mask_bits
        self.max_connections = max_connections

        self.address = self.__get_local_ip()

        self.connection_listener = socket(AF_INET, SOCK_STREAM)
        self.connection_listener.bind((self.address, self.messaging_port))

        self.broadcast_listener = socket(AF_INET, SOCK_DGRAM)
        self.broadcast_listener.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
        self.broadcast_listener.bind(("", self.broadcast_port))

        self.common_sender = socket(AF_INET, SOCK_DGRAM)
        self.common_sender.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)

        self.known_clients = {}
        self.connections = {}
        self.connection_threads = []
        self.running = False
        self.broadcast_thread = None
        self.connections_thread = None
        self.rsa_keys = None

    def start(self):
        logging.info('Generating RSA keys...')
        rsa_keys_generation_start_time = time()
        self.rsa_keys = rsa.newkeys(self.encryption_bits)
        logging.info(f'Generating RSA keys took {time() - rsa_keys_generation_start_time} seconds')

        self.say_broadcast(f'{_Message.HELLO.value} {self.username}')

        self.running = True
        self.broadcast_thread = Thread(target=self.listen_broadcast)
        self.broadcast_thread.start()
        self.connections_thread = Thread(target=self.listen_for_connections)
        self.connections_thread.start()

        logging.info('Successfully started')

    def stop(self):
        for active_connection in self.connections.items():
            active_connection[1]['socket'].send(f'{_Message.GOODBYE.value}  '.encode(self.encoding))

        self.running = False
        self.say_broadcast(_Message.GOODBYE.value)
        try:
            self.broadcast_listener.shutdown(SHUT_RDWR)
        except Exception:
            self.broadcast_listener.close()
        try:
            self.connection_listener.shutdown(SHUT_RDWR)
        except Exception:
            self.connection_listener.close()
        self.broadcast_thread.join()
        self.connections_thread.join()

        for connection in self.connections.items():
            connection[1]['socket'].close()
        for connection_thread in self.connection_threads:
            connection_thread.join()

        logging.info('Successfully stopped')

    def say_broadcast(self, message: str):
        self.common_sender.sendto(message.encode(self.encoding), self.__broadcast_address())

    def listen_for_connections(self):
        self.connection_listener.listen(self.max_connections)

        while self.running:
            socket_connection, address = self.connection_listener.accept()

            if address[0] in self.connections:
                continue

            self.connections[address[0]] = {
                'socket': socket_connection,
                'username': self.known_clients[address[0]] if address[0] in self.known_clients else address[0],
                'messages': []
            }
            logging.info(f'New connection with {address}')
            conn_thread = Thread(target=self.__handle_user_connection, args=[self.connections[address[0]], address[0]])
            self.connection_threads.append(conn_thread)
            conn_thread.start()

    def listen_broadcast(self):
        while self.running:
            try:
                message_and_address = self.broadcast_listener.recvfrom(self.broadcast_buff_size)
            except Exception:
                logging.error('Failed to listen broadcast')
                continue

            ip = message_and_address[1][0]
            if ip == self.address:
                continue

            message = message_and_address[0].decode(self.encoding).split(' ')
            if message is None or len(message) == 0:
                logging.warning(f'Received empty message from {ip}')
                continue

            match message[0]:
                case _Message.HELLO.value:
                    if len(message) < 2:
                        logging.warning(f'Incorrect message "{message[0]}" from {ip}')
                        continue

                    self.__add_user(ip, ' '.join(message[1:]))
                    self.broadcast_listener.sendto(f'{_Message.HELLO_BACK.value} {self.username}'.encode(self.encoding),
                                                   (ip, self.broadcast_port))
                case _Message.HELLO_BACK.value:
                    if len(message) < 2:
                        logging.warning(f'Incorrect message "{message[0]}" from {ip}')
                        continue
                    self.__add_user(ip, ' '.join(message[1:]))
                case _Message.GOODBYE.value:
                    if ip in self.known_clients:
                        del self.known_clients[ip]
                        logging.info(f'Client {ip} has left')

    def get_messages_by_ip(self, ip: str):
        if ip not in self.connections:
            return []
        try:
            return self.connections[ip]['messages']
        except Exception:
            return []

    def send_message_to_ip(self, ip: str, message: str):
        if ip not in self.connections:
            raise ValueError(f'{ip} is not in connections')
        if 'public_key' not in self.connections[ip]:
            raise ValueError(f'Client {ip} does not send his public key -> unable to send message')

        self.connections[ip]['socket'].send(f'{_Message.MESSAGE.value} '.encode(self.encoding) + rsa.encrypt(message.encode(self.encoding), self.connections[ip]["public_key"]))
        self.connections[ip]['messages'].append(f'Вы: {message}')
        return self.connections[ip]['messages'][-1]

    def connect(self, ip):
        if ip in self.connections:
            return

        if len(self.connections) >= self.max_connections:
            logging.warning('Unable to create new connection: exceeded maximum number of connections')
            return

        new_connection = {
            'socket': socket(AF_INET, SOCK_STREAM),
            'username': self.known_clients[ip] if ip in self.known_clients else ip,
            'messages': []
        }
        new_connection['socket'].connect((ip, self.messaging_port))
        logging.info(f'New connection with {ip}')
        self.connections[ip] = new_connection

        conn_thread = Thread(target=self.__handle_user_connection, args=[self.connections[ip], ip])
        self.connection_threads.append(conn_thread)
        conn_thread.start()

    def refresh_user_list(self):
        self.known_clients.clear()
        self.say_broadcast(f'{_Message.HELLO.value} {self.username}')

    def __handle_user_connection(self, connection, ip):
        connection['socket'].send(f'{_Message.KEY.value} {self.rsa_keys[0].n} {self.rsa_keys[0].e}'.encode(self.encoding))

        while self.running:
            try:
                message_bytes = connection['socket'].recv(self.message_buff_size)

                if message_bytes:
                    try:
                        first_space_index = message_bytes.index(' '.encode(self.encoding))
                    except Exception:
                        logging.warning(f'Incorrect message from {ip}')
                        continue

                    command = message_bytes[:first_space_index].decode(self.encoding)
                    message = message_bytes[(first_space_index + 1):]

                    if 'public_key' not in connection and command != _Message.KEY.value:
                        logging.warning(f'Client {ip} {connection["username"]} is trying to send message without sending public key')
                        continue

                    match command:
                        case _Message.KEY.value:
                            message = message.decode(self.encoding).split(' ')
                            if len(message) < 2:
                                logging.warning(f'Incorrect message from {ip} {connection["username"]}: {" ".join(message)}')
                                continue
                            connection['public_key'] = rsa.PublicKey(int(message[0]), int(message[1]))
                        case _Message.MESSAGE.value:
                            connection['messages'].append(f'{connection["username"]}: ' + rsa.decrypt(message, self.rsa_keys[1]).decode(self.encoding))
                            logging.info(f'{ip} {connection["username"]} - {connection["messages"][-1]}')
                        case _Message.GOODBYE.value:
                            connection['messages'].append(f'Клиент {connection["username"]} покидает чат')
                            break

                else:
                    logging.warning(f'Failed to decode message from {ip} {connection["username"]}')

            except DecryptionError as e:
                logging.error(f'Error to decode message: {e}')
            except Exception as e:
                logging.error(f'Error to handle user connection: {e}')
                self.__close_connection(connection, ip)
                break

        self.__close_connection(connection, ip)

    def __add_user(self, ip, username):
        if ip not in self.known_clients:
            self.known_clients[ip] = username
            logging.info(f'New known user: {ip}')

    def __close_connection(self, conn, ip):
        if self.running:
            sleep(1)
        conn['socket'].close()
        if ip in self.connections:
            del self.connections[ip]

    def __broadcast_address(self):
        return (str(IPv4Network(f'{self.address}/{self.mask_bits}', False).broadcast_address),
                self.broadcast_port)

    @staticmethod
    def __get_local_ip():
        ip_socket = socket(AF_INET, SOCK_DGRAM)
        try:
            ip_socket.connect(('10.0.0.0', 0))
            return ip_socket.getsockname()[0]
        except Exception as e:
            logging.error(f'Failed to get LAN IP-address: {e}')
            return '127.0.0.1'
        finally:
            ip_socket.close()
