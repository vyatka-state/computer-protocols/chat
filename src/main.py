from PyQt5.QtWidgets import QApplication, QInputDialog, QLineEdit
from dotenv import load_dotenv
from os import getenv
import logging
import sys
from frontend import Window


def main():
    logging.basicConfig(level='INFO')
    app = QApplication(sys.argv)
    load_dotenv()
    screen = Window(app.primaryScreen().size(), int(getenv('CHAT_MESSAGING_PORT')), int(getenv('CHAT_BROADCAST_PORT')),
                    int(getenv('CHAT_ADDRESS_MASK')))
    text, ok_pressed = QInputDialog.getText(screen, "Аутентификация", "Введите своё имя:", QLineEdit.Normal, "")
    if text is None or len(text) == 0 or not ok_pressed:
        return
    screen.start_chat(text)
    screen.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
