from PyQt5.QtWidgets import *
from PyQt5.Qt import Qt
from backend import SocketChat

from threading import Thread


class Window(QWidget):
    def __init__(self, screen_size, messaging_port: int, broadcast_port: int, mask_bits: int):
        QWidget.__init__(self)
        self.messaging_port = messaging_port
        self.broadcast_port = broadcast_port
        self.mask_bits = mask_bits

        self.setFixedWidth(int(screen_size.width() / 1.5))
        self.setFixedHeight(int(screen_size.height() / 1.5))
        self.setWindowTitle('Чат')

        layout = QGridLayout()
        self.setLayout(layout)

        self.contacts = QListWidget()
        self.contacts.itemDoubleClicked.connect(self.connect_with)
        layout.addWidget(self.contacts, 1, 1, 1, 2)

        self.refresh_button = QPushButton("Обновить контакты")
        self.refresh_button.clicked.connect(self.refresh_clicked)
        layout.addWidget(self.refresh_button, 0, 1, 1, 2)

        self.companion = QLabel()
        layout.addWidget(self.companion, 0, 0, 1, 1)

        self.text_edit_layout = QGridLayout()

        self.text_edit = QTextEdit()
        self.text_edit.setReadOnly(True)
        self.text_edit_layout.addWidget(self.text_edit, 0, 0, 1, 2)

        self.message_send = QLineEdit()
        self.text_edit_layout.addWidget(self.message_send, 1, 0)

        self.send_button = QPushButton("Отправить")
        self.send_button.clicked.connect(self.send_message)
        self.text_edit_layout.addWidget(self.send_button, 1, 1)

        layout.addLayout(self.text_edit_layout, 1, 0, 1, 1)

        self.chat = None
        self.current_connection = None
        self.check_new_clients = True
        self.check_new_messages = True
        self.messages_count = 0
        self.last_message_count = 0
        self.update_checker_thread = None

    def check_for_updates(self):
        while self.chat.running:
            if self.check_new_clients:
                if len(self.chat.known_clients) != self.contacts.count():
                    self.contacts.clear()
                    for client in self.chat.known_clients.items():
                        self.contacts.addItem(f'{client[1]} - {client[0]}')

            if self.check_new_messages and self.current_connection is not None:
                messages = self.chat.get_messages_by_ip(self.current_connection[1])
                if len(messages) - self.last_message_count < 0:
                    self.messages_count = 0
                self.last_message_count = len(messages)
                messages_count_diff = len(messages) - self.messages_count
                if messages_count_diff > 0:
                    for message in messages[(-messages_count_diff):]:
                        self.text_edit.append(message)
                        self.messages_count += 1

    def closeEvent(self, event):
        self.chat.stop()
        if self.update_checker_thread is not None:
            self.update_checker_thread.join()

    def keyPressEvent(self, QKeyEvent):
        if QKeyEvent.key() == Qt.Key_Return:
            self.send_message()

    def start_chat(self, username):
        self.chat = SocketChat(self.messaging_port, self.broadcast_port, self.mask_bits, username)
        self.chat.start()
        self.update_checker_thread = Thread(target=self.check_for_updates)
        self.update_checker_thread.start()

    def refresh_clicked(self):
        self.check_new_clients = False
        self.contacts.clear()
        self.chat.refresh_user_list()
        for client in self.chat.known_clients.items():
            self.contacts.addItem(f'{client[1]} - {client[0]}')
        self.check_new_clients = True

    def connect_with(self, item):
        self.check_new_messages = False
        self.text_edit.clear()
        self.current_connection = item.text().split(' - ')
        self.chat.connect(self.current_connection[1])
        self.companion.setText(item.text())

        self.messages_count = 0
        for message in self.chat.get_messages_by_ip(self.current_connection[1]):
            self.text_edit.append(message)
            self.messages_count += 1

        self.check_new_messages = True

        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setWindowTitle('Подключение')
        msg.setText(f"Вы успешно подключились к чату с {self.current_connection[0]}")
        msg.exec_()

    def send_message(self):
        if self.current_connection is None:
            return

        self.check_new_messages = False
        try:
            sent_message = self.chat.send_message_to_ip(self.current_connection[1], self.message_send.text())
            self.text_edit.append(sent_message)
            self.messages_count += 1
            self.message_send.clear()
            self.check_new_messages = True
        except Exception:
            self.check_new_messages = True
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Warning)
            msg.setWindowTitle('Ошибка отправки')
            msg.setText("Не удалось отправить сообщение")
            msg.exec_()
