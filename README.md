# Serverless socket chat

## How to run
All commands to be executed from project root

1) U need python 3.10
2) Create virtual environment
    ```
    python -m venv venv
    ```
3) Active virtual environment:
    - On Linux:
   ```
   source venv/bin/activate
   ```
   - On Windows:
   ```
   ./venv/Scripts/activate
   ```
4) Install required libraries
    ```
    pip install -r requirements.txt
    ```
5) Run chat
    ```
    python src/main.py
    ```